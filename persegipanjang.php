<?php
 class persegiPanjangClass{
	 public function keliling($p,$l){
		 return 2 * ($p + $l);
	 }
	 public function luas($p,$l){
		 return $p * $l;
	 }
 }
 $fun2 = new persegiPanjangClass;
 $pan = 6;
 $leb = 7;
?>
<html>
<head>
<title>Branch Developing</title>
<body>
  <h2>Panjang Persegi Panjang : <?= $pan ?> </h2>
  <h2>Lebar Persegi Panjang : <?= $leb ?> </h2>
  <h2>Keliling Persegi Panjang : <?= $fun2->keliling($pan,$leb) ?></h2>
  <h2>Luas Persegi Panjang : <?= $fun2->luas($pan,$leb) ?></h2>
</body>
</head>
</html>