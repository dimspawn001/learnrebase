<?php
 class kubusClass{
	 public function keliling($sisi){
		 return $sisi * 4;
	 }
	 public function luas($sisi){
		 return $sisi * $sisi;
	 }
 }
 $fun = new kubusClass;
 $sis = 6;
?>
<html>
<head>
<title>Branch Developing</title>
<body>
  <h2>Sisi Kubus : <?= $sis ?> </h2>
  <h2>Keliling Kubus : <?= $fun->keliling($sis) ?></h2>
  <h2>Luas Kubus : <?= $fun->luas($sis) ?></h2>
</body>
</head>
</html>